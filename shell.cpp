#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <cerrno>   // errno.h
#include <climits>  // limits.h
#include <cstdint>  // stdint.h
#include <cstdio>   // stdio.h
#include <cstdlib>  // stdlib.h
#include <cstring>  // string.h
#include <ctime>    // time.h
#include <libgen.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <vector>

#include "shell.h"

char * history_file = nullptr;
std::vector<char const *> history;
std::vector<bg_job_t> bg_jobs;

void usage(char * const program_name) {
    fprintf(stderr, "Usage: %s [-f <script file>] [-h <history file>]\n", program_name);
}

FILE * open_file(char const * const path, char const * const mode) {
    FILE * const stream = fopen(path, mode);
    if (stream == nullptr) {
        switch (errno) {
            case EEXIST:
            case EFAULT:
                fprintf(stderr, "Unexpected error: fopen called incorrectly\n");
                abort();
            case ENOMEM:
                fprintf(stderr, "Unexpected error: Out of memory\n");
                abort();
            case EACCES:
                fprintf(stderr, "Permission denied: %s\n", path);
                exit(EXIT_FAILURE);
            case EBUSY:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EDQUOT:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EFBIG:
            case EOVERFLOW:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EINTR:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EINVAL:
                fprintf(stderr, "Error: EINVAL (has many meanings) when opening: %s\n", path);
                exit(EXIT_FAILURE);
            case ELOOP:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EMFILE:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENAMETOOLONG:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENFILE:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENODEV:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENOENT:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENOSPC:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENOTDIR:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ENXIO:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EPERM:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case EROFS:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            case ETXTBSY:
                fprintf(stderr, "Error message not implemented\n");
                // TODO
                abort();
            default:
                fprintf(stderr, "Unexpected error\n");
                abort();
        }
    }
    return stream;
}

void sigint_handler(int signo) {
    // TODO: proper interactivity
}

[[noreturn]] void exit_shell(int status) {
    if (history_file != nullptr) {
        FILE * const history_stream = open_file(history_file, "a");
        for (char const * const command : history) {
            fwrite(command, sizeof(char), strlen(command), history_stream);
            fwrite("\n", sizeof(char), 1, history_stream);
            // TODO: error handling
        }
    }

    if (bg_jobs.empty()) {
        exit(status);
    }

    // step 1: check if any jobs have terminated already since the last check
    wait_bg_jobs();

    if (bg_jobs.empty()) {
        exit(status);
    }

    // step 2: send SIGTERM to all remaining jobs
    for (bg_job_t const & bg_job : bg_jobs) {
        if (kill(bg_job.pid, SIGTERM) != 0) {
            switch (errno) {
                case EINVAL:
                    fprintf(stderr, "Unexpected error: kill called incorrectly\n");
                    abort();
                case EPERM:
                    // TODO: change message
                    fprintf(stderr, "Error: EPERM\n");
                    abort();
                case ESRCH:
                    // may be a zombie, will be waited on
                    break;
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }
    }

    // step 3: give programs a reasonable amount of time to respond to SIGTERM;
    //         wait regularly and exit as soon as all jobs have exited
    struct timespec const interval = {0, 10000000};  // 10 ms
    for (int i = 0; i < 100; i++) {
        wait_bg_jobs();
        if (bg_jobs.empty()) {
            exit(status);
        }
        nanosleep(&interval, nullptr);
    }

    // step 4: send SIGKILL to all remaining jobs
    for (bg_job_t const & bg_job : bg_jobs) {
        if (kill(bg_job.pid, SIGKILL) != 0) {
            switch (errno) {
                case EINVAL:
                    fprintf(stderr, "Unexpected error: kill called incorrectly\n");
                    abort();
                case EPERM:
                    // TODO: change message
                    fprintf(stderr, "Error: EPERM\n");
                    abort();
                case ESRCH:
                    // may be a zombie, will be waited on
                    break;
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }
    }

    // step 5: wait bg jobs again, give enough time to avoid a race condition
    struct timespec const interval2 = {0, 1000};  // 1 μs
    for (int i = 0; i < 100; i++) {
        wait_bg_jobs();
        if (bg_jobs.empty()) {
            exit(status);
        }
        nanosleep(&interval2, nullptr);
    }

    // even SIGKILL didn't work
    fprintf(stderr, "Unable to kill background jobs\n");
    abort();
}

int builtin_command_exit(std::vector<char const *> const & argv) {
    if (argv.size() > 2) {
        fprintf(stderr, "Too many arguments.\n");
        return EXIT_FAILURE;
    }

    int exit_code;
    if (argv.size() == 2) {
        errno = 0;
        exit_code = (int)strtol(argv[1], nullptr, 10);
        if (errno == EINVAL) {
            fprintf(stderr, "Invalid argument: %s\n", argv[1]);
            return EXIT_FAILURE;
        } else if (errno == ERANGE) {
            fprintf(stderr, "Argument out of range: %s\n", argv[1]);
            return EXIT_FAILURE;
        }
    } else {
        exit_code = 0;
    }
    exit_shell(exit_code);
}

int builtin_command_cd(std::vector<char const *> const & argv) {
    if (argv.size() < 2) {
        fprintf(stderr, "Missing argument: <path>\n");
        return EXIT_FAILURE;
    } else if (argv.size() > 2) {
        fprintf(stderr, "Too many arguments.\n");
        return EXIT_FAILURE;
    }
    if (chdir(argv[1]) != 0) {
        switch (errno) {
            case EACCES:
                fprintf(stderr, "Permission denied: %s\n", argv[1]);
                return EXIT_FAILURE;
            case EFAULT:
                fprintf(stderr, "Unexpected error: chdir called incorrectly\n");
                abort();
            case EIO:
                fprintf(stderr, "Unexpected error: An I/O error occurred.\n");
                return EXIT_FAILURE;
            case ELOOP:
                fprintf(stderr, "Too many symbolic links encountered while resolving: %s\n", argv[1]);
                return EXIT_FAILURE;
            case ENAMETOOLONG:
                fprintf(stderr, "Path too long: %s\n", argv[1]);
                return EXIT_FAILURE;
            case ENOENT:
                fprintf(stderr, "%s: No such file or directory\n", argv[1]);
                return EXIT_FAILURE;
            case ENOMEM:
                fprintf(stderr, "Unexpected error: Out of memory\n");
                abort();
            case ENOTDIR:
                fprintf(stderr, "Not a directory: %s\n", argv[1]);
                return EXIT_FAILURE;
            default:
                fprintf(stderr, "Unexpected error\n");
                abort();
        }
    }
    return EXIT_SUCCESS;
}

int builtin_command_history(std::vector<char const *> const & argv) {
    if (argv.size() > 1) {
        fprintf(stderr, "Too many arguments.\n");
        return EXIT_FAILURE;
    }
    for (int i = 0; i < history.size(); i++) {
        printf("%d\t%s\n", i, history[i]);
    }
    return EXIT_SUCCESS;
}

int builtin_rerun_history_command(std::vector<char const *> const & argv) {
    if (argv.size() > 1) {
        fprintf(stderr, "Too many arguments.\n");
        return EXIT_FAILURE;
    }
    errno = 0;
    long n = strtol(argv[0] + 1, nullptr, 10);
    if (errno == EINVAL) {
        fprintf(stderr, "Invalid integer: %s\n", argv[0] + 1);
        return EXIT_FAILURE;
    } else if (errno == ERANGE) {
        fprintf(stderr, "Integer out of range: %s\n", argv[0] + 1);
        return EXIT_FAILURE;
    }
    if (n < 0 || n >= history.size()) {
        fprintf(stderr, "Invalid history index: %s\n", argv[0] + 1);
        return EXIT_FAILURE;
    }
    puts(history[n]);
    return execute_command(history[n]);
}

int builtin_command_ps(std::vector<char const *> const & argv) {
    if (argv.size() > 1) {
        fprintf(stderr, "Too many arguments.\n");
        return EXIT_FAILURE;
    }
    puts("PID\tCOMMAND");
    for (bg_job_t const & job : bg_jobs) {
        std::string command_str;
        for (std::string const & token : job.argv) {
            if (!command_str.empty()) {
                command_str += ' ';
            }
            command_str += token;
        }
        printf("%jd\t%s\n", (intmax_t)job.pid, command_str.c_str());
    }
    return EXIT_SUCCESS;
}

void wait_bg_jobs() {
    for (int i = 0; i < bg_jobs.size(); i++) {
        int wstatus;
        pid_t wresult = waitpid(bg_jobs[i].pid, &wstatus, WNOHANG);
        if (wresult == 0) {
            // process hasn't finished
            continue;
        } else if (wresult == -1) {
            switch (errno) {
                case ECHILD:
                case EINTR:
                case EINVAL:
                    fprintf(stderr, "Unexpected error: waitpid called incorrectly\n");
                    abort();
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }
        // printf("Job %jd finished!", (intmax_t)bg_jobs[i].pid);
        bg_jobs.erase(bg_jobs.begin() + i);
    }
}

int rerun_prefix_match(char const * const prefix) {
    size_t prefix_len = strlen(prefix);
    if (history.empty()) {
        puts("No Match");
        return EXIT_FAILURE;
    }
    for (ssize_t i = history.size() - 1; i >= 0; i--) {
        if (strlen(history[i]) >= prefix_len && memcmp(prefix, history[i], prefix_len) == 0) {
            puts(history[i]);
            return execute_command(history[i]);
        }
    }
    puts("No Match");
    return EXIT_FAILURE;
}

void add_to_history(char const * const command, size_t command_str_size) {
    char * const history_command = (char *)malloc(command_str_size);
    if (history_command == nullptr) {
        switch (errno) {
            case ENOMEM:
                fprintf(stderr, "Unexpected error: Out of memory\n");
                abort();
            default:
                fprintf(stderr, "Unexpected error\n");
                abort();
        }
    }
    memcpy(history_command, command, command_str_size);
    history.push_back(history_command);
}

int execute_command(char const * const command_str) {
    size_t const command_str_size = strlen(command_str) + 1;

    // make a mutable copy
    char command_str_cpy[command_str_size];
    memcpy(command_str_cpy, command_str, command_str_size);

    // && logical operator
    {
        char * const substr = strstr(command_str_cpy, "&&");
        if (substr != nullptr) {
            substr[0] = '\0';
            char const * command_1_str = command_str_cpy;
            char const * command_2_str = &substr[2];
            int status_1 = execute_command(command_1_str);
            if (status_1 == 0) {
                return execute_command(command_2_str);
            } else {
                return status_1;
            }
        }
    }
    // || logical operator
    {
        char * const substr = strstr(command_str_cpy, "||");
        if (substr != nullptr) {
            substr[0] = '\0';
            char const * command_1_str = command_str_cpy;
            char const * command_2_str = &substr[2];
            int status_1 = execute_command(command_1_str);
            if (status_1 == 0) {
                return status_1;
            } else {
                return execute_command(command_2_str);
            }
        }
    }
    // ; separator
    {
        char * const substr = strstr(command_str_cpy, ";");
        if (substr != nullptr) {
            substr[0] = '\0';
            char const * command_1_str = command_str_cpy;
            char const * command_2_str = &substr[1];
            execute_command(command_1_str);
            return execute_command(command_2_str);
        }
    }

    // parse input into tokens
    std::vector<char const *> tokens;
    for (char const * token = strtok(command_str_cpy, " "); token != nullptr; token = strtok(nullptr, " ")) {
        tokens.push_back(token);
    }

    if (tokens.empty()) {
        return EXIT_SUCCESS;
    }

    if (strcmp(tokens[0], "exit") == 0) {
        return builtin_command_exit(tokens);
    } else if (strcmp(tokens[0], "cd") == 0) {
        add_to_history(command_str, command_str_size);
        return builtin_command_cd(tokens);
    } else if (strcmp(tokens[0], "!history") == 0) {
        return builtin_command_history(tokens);
    } else if (tokens[0][0] == '#') {
        return builtin_rerun_history_command(tokens);
    } else if (tokens[0][0] == '!') {
        return rerun_prefix_match(&command_str[1]);
    } else if (strcmp(tokens[0], "ps") == 0) {
        return builtin_command_ps(tokens);
    } else {
        // execute external command

        add_to_history(command_str, command_str_size);

        // trailing & means background
        bool foreground = strcmp(tokens.back(), "&") != 0;

        // construct argv
        size_t argv_len = foreground ? tokens.size() : tokens.size() - 1;
        char * argv[argv_len + 1];
        memcpy(argv, tokens.data(), argv_len * sizeof(char *));
        argv[argv_len] = nullptr;

        pid_t new_pid = fork();
        if (new_pid == -1) {  // error
            switch (errno) {
                // TODO
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        } else if (new_pid == 0) {  // child

            // sigint handler
            struct sigaction sigint_handler = {};
            sigint_handler.sa_handler = SIG_DFL;
            sigaction(SIGINT, &sigint_handler, nullptr);

            printf("Command executed by pid=%jd\n", (intmax_t)getpid());

            execvp(tokens[0], argv);

            // this code is only reachable if exec failed
            switch (errno) {
                case E2BIG:
                    fprintf(stderr, "Too many arguments (E2BIG)\n");
                    break;
                case EACCES:
                case EPERM:
                    fprintf(stderr, "Permission denied: %s\n", tokens[0]);
                    break;
                case EAGAIN:
                    fprintf(stderr, "Error: rlimit reached.\n");
                    break;
                case EFAULT:
                    fprintf(stderr, "Unexpected error: execvp called incorrectly\n");
                    break;
                case EIO:
                    fprintf(stderr, "Unexpected error: An I/O error occurred.\n");
                    break;
                case EISDIR:
                    fprintf(stderr, "Error: ELF interpreter was a directory.s\n");
                    break;
                case ELIBBAD:
                    fprintf(stderr, "Error: ELF interpreter was not in a recognized format.\n");
                    break;
                case ELOOP:
                    fprintf(stderr, "Error: Too many symbolic links encountered or maximum recursion limit was reached "
                                    "during recursive script interpretation.\n");
                    break;
                case EMFILE:
                    fprintf(stderr, "Error: The per-process limit on the number of open file descriptors has been "
                                    "reached.\n");
                    break;
                case ENAMETOOLONG:
                    fprintf(stderr, "Path too long: %s\n", tokens[0]);
                    break;
                case ENFILE:
                    fprintf(stderr, "Error: The system-wide limit on the total number of open files has been "
                                    "reached.\n");
                    break;
                case ENOENT:
                    fprintf(stderr, "Error: File not found.\n");
                    break;
                case ENOEXEC:
                    fprintf(stderr, "Error: Executable format error.\n");
                    break;
                case ENOMEM:
                    fprintf(stderr, "Unexpected error: Out of memory\n");
                    break;
                case ENOTDIR:
                    fprintf(stderr, "Error: Not a directory.\n");
                    break;
                case ETXTBSY:
                    fprintf(stderr, "Error: File busy: %s\n", tokens[0]);
                    break;
                default:
                    fprintf(stderr, "Unexpected error\n");
            }
            _exit(EXIT_FAILURE);
        } else {  // parent
            if (foreground) {
                int wstatus;
                pid_t wresult = waitpid(new_pid, &wstatus, 0);
                if (wresult == -1) {
                    switch (errno) {
                        case ECHILD:
                        case EINVAL:
                            fprintf(stderr, "Unexpected error: waitpid called incorrectly\n");
                            abort();
                        case EINTR:
                            fprintf(stderr, "INTERRUPT (implementation not finished)\n");
                            // TODO
                        default:
                            fprintf(stderr, "Unexpected error\n");
                            abort();
                    }
                }
                if (WIFEXITED(wstatus)) {
                    return WEXITSTATUS(wstatus);
                } else if (WIFSIGNALED(wstatus)) {
                    return 128 + WTERMSIG(wstatus);
                } else {
                    fprintf(stderr, "Unexpected waitpid behavior\n");
                    abort();
                }
            } else {
                std::vector<std::string> argv_vector;
                argv_vector.reserve(argv_len);
                for (int i = 0; i < argv_len; i++) {
                    argv_vector.emplace_back(argv[i]);
                }
                bg_jobs.push_back({
                    .pid=new_pid,
                    .argv=argv_vector
                });
                return EXIT_SUCCESS;
            }
        }
    }
}

void run_script(char const * const script_file) {
    FILE * const script_stream = open_file(script_file, "r");
    char * line = nullptr;
    size_t size = 0;
    while (true) {
        errno = 0;
        ssize_t bytes_read = getline(&line, &size, script_stream);
        if (bytes_read == -1) {
            free(line);
            if (errno == 0) {
                // reached end of file
                break;
            }
            switch (errno) {
                case EINVAL:
                    fprintf(stderr, "Unexpected error: getline called incorrectly\n");
                    abort();
                case ENOMEM:
                    fprintf(stderr, "Unexpected error: memory allocation failed\n");
                    abort();
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        } else {
            // the string includes the newline at the end and we don't want that,
            // so overwrite the newline with a null byte
            line[bytes_read - 1] = '\0';

            // note: if the line contains a null byte that will just be treated as the end of the line
            execute_command(line);
        }
    }
    exit_shell(EXIT_SUCCESS);
}

[[noreturn]] void prompt() {
    intmax_t self_pid = getpid();
    char cwd[PATH_MAX + 1];
    std::string command_str;
    while (true) {

        // get cwd
        if (getcwd(cwd, sizeof(cwd)) == nullptr) {
            switch (errno) {
                case EACCES:
                    fprintf(stderr, "Error displaying prompt: Permission denied to get the current working directory\n");
                    exit_shell(EXIT_FAILURE);
                case EFAULT:
                case EINVAL:
                case ERANGE:  // should not be possible because size is passed as PATH_MAX+1
                    fprintf(stderr, "Unexpected error: getcwd called incorrectly\n");
                    abort();
                case ENAMETOOLONG:
                    fprintf(stderr, "Error displaying prompt: Current working directory path name too long\n");
                    exit_shell(EXIT_FAILURE);
                case ENOENT:
                    fprintf(stderr, "Error displaying prompt: Current working directory is gone\n");
                    exit_shell(EXIT_FAILURE);
                case ENOMEM:
                    fprintf(stderr, "Unexpected error: Out of memory\n");
                    abort();
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }

        // display prompt
        printf("(pid=%jd)%s$ ", self_pid, cwd);

        // get input
        std::getline(std::cin, command_str);
        // printf("getline returned, eof:%d, empty:%x, fail:%d, bad:%d\n", std::cin.eof(), command_str.empty(),
        //        std::cin.fail(), std::cin.bad());
        // for (int i = 0; i <= command_str.size(); i++) {
        //     printf("%02x", command_str.c_str()[i]);
        // }

        // this needs to be run regularly and this is a decent place
        wait_bg_jobs();

        if (std::cin.eof() && command_str.empty()) {
            putchar('\n');
            exit_shell(EXIT_SUCCESS);
        }
        if (std::cin.fail() || std::cin.bad()) {
            if (std::cin.eof()) {
                putchar('\n');
                std::cin.clear();
                continue;
            }
            // TODO: better error handling
            fprintf(stderr, "Unexpected error reading from stdin\n");
            abort();
        }

        execute_command(command_str.c_str());
    }
}

int main(int const argc, char * const argv[]) {

    // parse arguments
    char * script_file = nullptr;
    int opt;
    while ((opt = getopt(argc, argv, "f:h:")) != -1) {
        switch (opt) {
            case 'f':
                script_file = optarg;
                break;
            case 'h':
                history_file = optarg;
                break;
            default:
                fprintf(stderr, "Unexpected argument: -%c\n", opt);
                usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (history_file != nullptr) {
        // cursory validation of history_file

        // make a copy because dirname may modify the string
        size_t history_file_strlen = strlen(history_file);
        char history_file_cpy[history_file_strlen + 1];
        memcpy(history_file_cpy, history_file, history_file_strlen + 1);

        char const * const history_file_dir = dirname(history_file_cpy);
        struct stat history_file_dir_stat = {};
        if (stat(history_file_dir, &history_file_dir_stat) != 0) {
            switch (errno) {
                case EACCES:
                    fprintf(stderr, "Permission denied: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                case EFAULT:
                    fprintf(stderr, "Unexpected error: Bad address\n");
                    abort();
                case ELOOP:
                    fprintf(stderr, "Too many symbolic links encountered while traversing: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                case ENAMETOOLONG:
                    fprintf(stderr, "Path too long: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                case ENOENT:
                    fprintf(stderr, "Directory not found: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                case ENOMEM:
                    fprintf(stderr, "Unexpected error: Out of memory\n");
                    abort();
                case ENOTDIR:
                    fprintf(stderr, "A component of the path is not a directory: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                case EOVERFLOW:
                    fprintf(stderr, "Unexpected error: Overflow error when inspecting directory: %s\n", history_file_dir);
                    exit(EXIT_FAILURE);
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }
        if (!S_ISDIR(history_file_dir_stat.st_mode)) {
            fprintf(stderr, "Not a directory: %s\n", history_file_dir);
            exit(EXIT_FAILURE);
        }

        if (access(history_file, R_OK | W_OK) != 0) {
            switch (errno) {
                case EACCES:
                    fprintf(stderr, "Permission denied: %s\n", history_file);
                    exit(EXIT_FAILURE);
                case ELOOP:
                    fprintf(stderr, "Too many symbolic links encountered while traversing: %s\n", history_file);
                    exit(EXIT_FAILURE);
                case ENAMETOOLONG:
                    fprintf(stderr, "Path too long: %s\n", history_file);
                    exit(EXIT_FAILURE);
                case ENOENT:
                    // acceptable; file will be created
                    break;
                case ENOTDIR:
                    fprintf(stderr, "A component of the path is not a directory: %s\n", history_file);
                    exit(EXIT_FAILURE);
                case EROFS:
                    fprintf(stderr, "Read-only filesystem: %s\n", history_file);
                    exit(EXIT_FAILURE);
                case EFAULT:
                case EINVAL:
                    fprintf(stderr, "Unexpected error: Bad address\n");
                    abort();
                case EIO:
                    fprintf(stderr, "Unexpected error: An I/O error occurred.\n");
                    exit(EXIT_FAILURE);
                case ENOMEM:
                    fprintf(stderr, "Unexpected error: Out of memory\n");
                    abort();
                case ETXTBSY:
                    // acceptable. file is busy; maybe it won't be later
                    break;
                default:
                    fprintf(stderr, "Unexpected error\n");
                    abort();
            }
        }
    }

    signal(SIGINT, sigint_handler);

    if (script_file == nullptr) {
        prompt();
    } else {
        run_script(script_file);
    }
}
