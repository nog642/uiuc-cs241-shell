#ifndef UIUCCS241SHELL_SHELL_H
#define UIUCCS241SHELL_SHELL_H

void usage(char *);
FILE * open_file(char const *, char const *);

typedef struct {
    pid_t pid;
    std::vector<std::string> argv;
} bg_job_t;

int builtin_command_exit(std::vector<char const *> const &);
int builtin_command_cd(std::vector<char const *> const &);
int builtin_command_history(std::vector<char const *> const &);
int builtin_rerun_history_command(std::vector<char const *> const &);

[[noreturn]] void exit_shell(int);
void add_to_history(char const *, size_t);
void wait_bg_jobs();
int rerun_prefix_match(char const *);
int execute_command(char const *);
void run_script(char const *);
[[noreturn]] void prompt();

#endif //UIUCCS241SHELL_SHELL_H
